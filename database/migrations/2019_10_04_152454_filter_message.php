<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FilterMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filter_message', function (Blueprint $table) {
                         $table->increments('iId');
                         $table->integer("grade")->nullable()->comment('級數');
                         $table->integer("msgNo")->nullable()->comment('簡訊流水號');
                         $table->string('msgIndex')->nullable()->comment('簡訊識別碼');
                         $table->string('msgPhoneNumber')->nullable()->comment('手機號碼');
                         $table->string('msgContent')->nullable()->comment('簡訊內容');
                         $table->string('msgCreatedTime')->nullable()->comment('建立時間');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
