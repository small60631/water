@extends('_web._layouts.main')



<!-- content -->
@section('content')
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">


        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
    @include('_web._layouts.breadcrumb')
    <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <!-- basic table -->
            {{--            <h2 style="font-size: 72px;color: red">i am the best</h2>--}}


            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                                <thead>
                                        <tr>
                                            <th class="th-sm">地震級數</th>
                                            <th class="th-sm">No</th>
                                            <th class="th-sm">簡訊識別碼</th>
                                            <th class="th-sm">簡訊手機碼</th>
                                            <th class="th-sm">簡訊內容</th>
                                            <th class="th-sm">簡訊建立時間</th>
                                        </tr>
                                </thead>

                                        @foreach($filter_reservoir as $Res)


                                        <tr>
                                            <td>{{$Res->grade}}</td>
                                                <td>{{$Res->msgNo}}</td>
                                                <td>{{$Res->msgIndex}}</td>
                                                <td>{{$Res->msgPhoneNumber}}</td>
                                                <td>{{$Res->msgContent}}</td>
                                                <td>{{$Res->msgCreatedTime}}</td>
                                        </tr>

                                        @endforeach

                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
@endsection


@section('aside')

@endsection
@section('inline-js')

    <script>
        // Basic example
        $(document).ready(function () {
            $('#dtBasicExample').DataTable({
            });
            $('.dataTables_length').addClass('bs-select');
        });
    </script>
@endsection

