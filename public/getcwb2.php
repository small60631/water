<!-- -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <title>地震豪雨通報系統</title>
    <link rel="stylesheet" href="reservoir2/dist/style/style.min.css">
    <!-- Favicon icon -->
<!--    <link rel="icon" type="image/png" sizes="16x16" href="xtreme-admin/assets/images/favicon.png">-->
    <link rel="icon" type="image/png" sizes="16x16" href="xtreme-admin/assets/images/logo.png">
<!--    <link type="text/css" rel="stylesheet" href="css/waitMe.css">-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script src="reservoir2/dist/script/lodash.min.js" type="text/javascript"></script>
    <script src="reservoir2/dist/script/script.js" type="text/javascript"></script>
</head>
<body>
<div class="page">
    <div class="nav">
        <div class="logo">
            <img src="reservoir2/dist/image/logo.svg" alt="logo">
        </div>
        <div class="btn-hamburger">
            <span class="hamburger"></span>
        </div>
        <ul class="nav-list">
            <li class="login">
                <a href="javascript:void(0);" class="btn btn-login" onclick="history.go(-1);">返回</a>
            </li>
        </ul>
    </div>
    <div class="main">
<?php
        $url = "https://opendata.cwb.gov.tw/api/v1/rest/datastore/E-A0015-001?Authorization=CWB-CB919A32-D34F-4771-B5BB-8BCD5D3CD0D0&limit=1&format=JSON";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);        
        curl_close($ch);
        
        $result = json_decode($result);
        $r1 = $result->{'records'};
        $r2 = $r1->{'earthquake'};
        $reportImageURI = $r2[0]->{'reportImageURI'};
        $shakemapImageURI = $r2[0]->{'shakemapImageURI'};
        
        // echo "<img src='$reportImageURI'><br>";
        echo "<img src='$shakemapImageURI'><br>"; 
?>

    </div>
</div>
</body>
</html>
