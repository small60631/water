<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reservoirMessage extends Model
{
    public $timestamps = false;
    protected $table = 'reservoir-messages';
    protected $primaryKey = 'msgNo';
    protected $fillable = [
      'msgIndex','msgPhoneNumber','msgContent','msgCreatedTime','msgInsertTime'
    ];
}
