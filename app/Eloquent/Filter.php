<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Filter extends Model
{
    public $timestamps = false;
    protected $table = 'filter_message';
    protected $primaryKey = 'iId';
    protected $fillable = [
        'grade','msgNo','msgIndex','msgPhoneNumber','msgContent','msgCreatedTime'
    ];
}
